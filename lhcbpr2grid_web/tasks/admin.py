from django.contrib import admin

# Register your models here.
from .models import Task, Stage, DiracJob

admin.site.register(Task)
admin.site.register(Stage)
admin.site.register(DiracJob)
