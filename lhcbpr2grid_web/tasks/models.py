from django.db import models

# ProductionTask is a single submission to Ganga of either a Job or Task
class Task(models.Model):
    task_name = models.CharField(max_length=500)
    task_submit_date = models.DateTimeField("submit date")
    curr_status = models.CharField(max_length=100)
    ganga_task_id = models.IntegerField(default=-1)
    status_info = models.CharField(max_length=3000)

    def __str__(self):
        ret_str = self.task_name + " ({})".format(self.curr_status)
        return ret_str
    
class Stage(models.Model):
    stage_name = models.CharField(max_length=500)
    stage_submit_date = models.DateTimeField("submit date")
    curr_status = models.CharField(max_length=100)
    ganga_job_id = models.IntegerField(default=-1)
    ganga_transform_id = models.IntegerField(default=-1)
    num_dirac_jobs = models.IntegerField(default=-1)
    task_parent = models.ForeignKey(Task, on_delete=models.CASCADE)
    status_info  = models.CharField(max_length=3000)

    def __str__(self):
        ret_str = self.stage_name + " ({})".format(self.curr_status)
        return ret_str

class DiracJob(models.Model):
    dirac_id = models.IntegerField(default=-1)
    job_status = models.CharField(max_length=100)
    stage_parent = models.ForeignKey(Stage, on_delete=models.CASCADE)

    def __str__(self):
        ret_str = int(self.dirac_id) + " ({})".format(self.job_status)
        return ret_str
