from django.shortcuts import render
from django.http import HttpResponse
from .models import Task, Stage, DiracJob
from django.shortcuts import render

# Create your views here.
def index(request):
    latest_task_list = Task.objects.order_by("-task_submit_date")[:5]
    
    context = {"latest_task_list": latest_task_list}
    return render(request, "tasks/index.html", context)
    
def detail(request, task_id):
    try:
        task = Task.objects.get(pk=task_id)
    except Task.DoesNotExist:
        raise Http404("Task does not exist")
    return render(request, "tasks/detail.html", {"task": task})


    
