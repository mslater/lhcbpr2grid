'''
File setup to extract the input for the specific moore job "DC_Sim10aU1_MinBias_def_Allen"

Different versions of these files need to be made for testing different PR jobs, hence why
the user needs to provide this/ just give an input file list instead.
'''

from Moore import options
from PRConfig.FilesFromDirac import get_access_urls


options.input_files = get_access_urls(
    "/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1",
    "30000000", ["XDIGI"])

print(str(options.input_files[:10]))