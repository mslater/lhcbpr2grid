###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.hlt1_allen import make_allen_forward_tracks
from RecoConf.mc_checking import check_tracking_efficiency, make_links_tracks_mcparticles, make_links_lhcbids_mcparticles_tracking_system
from RecoConf.mc_checking_categories import get_mc_categories, get_hit_type_mask
from AllenConf.velo_reconstruction import decode_velo


def hlt1_reco_allen_tracks():

    # make links to lhcb id for mc matching
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()

    # build the PrChecker algorihm for forward track
    forward_tracks = make_allen_forward_tracks()
    links_to_forward_tracks = make_links_tracks_mcparticles(
        InputTracks=forward_tracks['v1keyed'], LinksToLHCbIDs=links_to_lhcbids)

    pr_checker_for_forward_track = check_tracking_efficiency(
        TrackType="Forward",
        InputTracks=forward_tracks['v1keyed'],
        LinksToTracks=links_to_forward_tracks,
        LinksToLHCbIDs=links_to_lhcbids,
        MCCategories=get_mc_categories("MuonMatch"),
        HitTypesToCheck=get_hit_type_mask("BestLong"),
    )

    return Reconstruction('allen_reco', [pr_checker_for_forward_track],
                          [require_gec()])


options.histo_file = "hlt1_allen_track_reconstruction.root"
with decode_velo.bind(retina_decoding=False):
    run_allen_reconstruction(options, hlt1_reco_allen_tracks)
