#
import subprocess
import time
import os
import sys
import numpy as np


##Aims
# make this all work by tasks!
# all in one event loop if possible

def PFNToLFN(PFNList):
    '''
    Function to convert PFN to LFN:

    Runs a dirac command to get a PFN's metadata and extracts from the output the corresponding LFN

    Places these new LFNs into a file that is then read back in to the ganga job.

    Clean up of these files performed at the end

    Attributes:
    PFNList : List of PFNs
    '''
    diracCommand=f"lb-dirac dirac-dms-pfn-metadata {' '.join(PFNList)} &> PFNToLFNOut.txt"

    if os.path.exists("LFNOut.txt"):
        os.remove("LFNOut.txt")

    #this could definitely just be a function in a file, but allows this to be run
    # internally without extra files
    extractLFNScript = ['LFNList=[]',
                        f'with open("PFNToLFNOut.txt","r") as f:',
                        '   Lines = f.readlines()',
                        '   extractNext=False',
                        '   for line in Lines:',
                        '       line=line.strip()',
                        '       if "Successful :" in line or "Writeable :" in line:',
                        '           extractNext=True',
                        '       elif extractNext==True:',
                        '           extractNext=False',
                        '           if line.replace(" ","")[0]=="/":',
                        '               LFNList.append(line.replace(" ","").replace(":",""))',
                        f'with open("LFNOut.txt","w") as f:',
                        '   for lfn in LFNList:',
                        r'      f.write(f"{lfn}\n")']
    newLineChar="\n"

    extractLFNCommand = f"python -c '{newLineChar.join(extractLFNScript)}'"




    rc=subprocess.Popen(f"{diracCommand};{extractLFNCommand}",shell=True)
    #runs and creates LFN list

    start=0
    while not os.path.exists("LFNOut.txt"):
        if start>60*60:
            print("Failed to convert PFNs to LFNs, exiting...")
            return None
        time.sleep(5)
        start+=5

    with open("LFNOut.txt") as f:
        LFNs = f.read().splitlines()

    #cleanup
    if os.path.exists("LFNOut.txt"):
        os.remove("LFNOut.txt")
    if os.path.exists("PFNToLFNOut.txt"):
        os.remove("PFNToLFNOut.txt")

    return LFNs


convListStrToList = lambda fileList : fileList[1:-1].replace(" ","").replace("'","").replace('"','').split(",")

convertPFNToLFN = True

#we work backwards, so "0" is the last job, "-1" the second last etc.

# Load in the (final) job options
jobOptions={"0":{a[0].rstrip().strip():a[1].rstrip().strip() for a in np.loadtxt(sys.argv[-1],delimiter=":",dtype=str)}}

#priorJobOptions = {}
a=0
if jobOptions["0"]["requiresJob"]!="":
    jobOptions["-1"] = {a[0].rstrip().strip():a[1].rstrip().strip() for a in np.loadtxt(sys.argv[-1][:-4]+"_-1.txt",delimiter=":",dtype=str)}
    a=-1
    while jobOptions[f"{a}"]["requiresJob"]!="":
        a-=1
        jobOptions[f"{a}"] = {a[0].rstrip().strip():a[1].rstrip().strip() for a in np.loadtxt(sys.argv[-1][:-4]+f"_{a}.txt",delimiter=":",dtype=str)}
initialJobKey = f"{a}"

##now to write this generically to work for normal job or chained jobs!
#example job setup



#set max number of jobs to run as num steps * num splits
t = LHCbTask(name=jobOptions["0"]["jobTitle"],
                float=(len(jobOptions))*(10 if jobOptions[initialJobKey]["splitJob"]=="True" else 1 if jobOptions[initialJobKey]["splitJob"]=="False" else int(jobOptions[initialJobKey]["splitJob"])))

##want this to be backwards ordered
keyList = [str(a) for a in range(0,-1*len(jobOptions),-1)]
#keyList.sort()

#start with the final job, end with the initial
jobs = [LHCbTransform(name = f"{jobOptions[key]['taskOrder']}") for key in keyList]

#add it now so ID is available
for j in jobs[::-1]:
    t.appendTransform(j)

local=False # global setting to control whether we run in local test mode or on the grid!
keepROOTFilesLocal=True #global setting to currently keep the root files local to not clog dan's dirac storage

print(keyList)

#"job" key and "job"
for i, (jobKey,j) in enumerate(zip(keyList,jobs)):

    myApp = GaudiExec()
    myApp.directory = f'{jobOptions[jobKey]["newDir"]}/{jobOptions[jobKey]["newDevDir"]}'

    j.application = myApp
    j.application.options = jobOptions[jobKey]["PRJobOptions"].split()
    #converts space separated string into a list of options
    #example job setup
    j.application.platform =  jobOptions[jobKey]["platform"]


    #sort out if we are working with the intial job
    if jobKey==initialJobKey:
        splitByFile=True
        if jobOptions[jobKey]["inputFiles"]=="[]":
            if os.path.exists(f"{jobOptions[jobKey]['newDir']}/jobInput.txt"):

                with open(f"{jobOptions[jobKey]['newDir']}/jobInput.txt","r") as f:
                    for a in f.readlines():
                        strippedA = a.strip("\n").strip()
                        if strippedA[0]=="[" and strippedA[-1]=="]":
                            inputData=convListStrToList(strippedA)
                            break
                    else:
                        print(f"No suitable file-list found in '{jobOptions[jobKey]['newDir']}/jobInput.py', aborting.",file=sys.stderr)
                        sys.exit(1)
                    
                    #Read file list from file, removing the formatting from printing
            else:
                ## we are assuming this is a no input job!
                ## need to turn the mc_num_units on for first file!
                inputData = None
                splitByFile=False
                splitString='--option="from Configurables import LHCbApp;LHCbApp().EvtMax = LHCbApp().EvtMax{0};"'
                splitStringRemainder = '--option="from Configurables import LHCbApp;LHCbApp().EvtMax += LHCbApp().EvtMax{0};"'
        #else read directly from the job options
        else:
            inputData=convListStrToList(jobOptions[jobKey]["inputFiles"])
        # inputData can now either be a dataset, a list of PFNs or a list of LFNs

        if inputData is not None and len(inputData)==1:
            inputData=inputData[0]

        data=None
        #figure out how to setup data
        if isinstance(inputData,str):
            #could be a dataset
            try:
                #if input is a bookkeeping entry
                data  = BKQuery(inputData, dqflag=['OK']).getDataset(compressed=False)
            except:
                #if input data is just one file
                inputData=[inputData]

        if data is None and inputData is not None:
            # if input files are already lfns
            if inputData[0][:6]=="/lhcb/":
                data = LHCbDataset(files=[DiracFile(lfn=a) for a in inputData],)
            elif inputData[0][:4]=="lfn:":
                data = LHCbDataset(files=[DiracFile(lfn=a[4:]) for a in inputData],)
            
            else:
                #input files are pfns
                if convertPFNToLFN:
                    #use converter to get more grid site options
                    data = LHCbDataset(files=[DiracFile(lfn=a) for a in PFNToLFN(inputData)],)
                else:
                    #don't
                    data = LHCbDataset(files=[PhysicalFile(a) for a in inputData],)

        #add any prepared input to this job!
        if inputData is not None:
            j.addInputData(data)
            j.files_per_unit=-1
        else:
            j.mc_num_units=1
        
        j.submit_with_threads = True

    else:
        splitByFile=False
        #now we deal with the chained jobs inputs
        inputDataFileMasks=[a.lower() for a in convListStrToList(jobOptions[jobKey]["inputFiles"])]

        
        j.addInputData(TaskChainInput(include_file_mask = [f'\{inputDataFileMask}$' for inputDataFileMask in inputDataFileMasks],
                                               input_trf_id = jobs[i+1].getID()))
        
        prevOutputFileTypes = [a.lower() for a in convListStrToList(jobOptions[f"{int(jobKey)-1}"]["outputFileTypes"])]
        
        for _ in inputDataFileMasks:
            if _ in prevOutputFileTypes:
                break
        else:
            #only delete chain input if not in the kept output files of previous
            # MWS: Caused problems
            #j.delete_chain_input = True
            pass

        j.submit_with_threads = True
        j.files_per_unit=-1

    #Now prepare output file types and storage

    #Continue commenting then make MD recipe and upload to a gitlab
    outputFileTypes=[a for a in convListStrToList(jobOptions[jobKey]["outputFileTypes"])]
    
    
    if jobOptions[jobKey]["taskOrder"]!="0":
        #we need to save the output as the input for the next job... the previous in the loop
        inputDataFileMasksPrevStep=[a.lower() for a in convListStrToList(jobOptions[str(int(jobKey)+1)]["inputFiles"])]
        outputFileTypes.extend(inputDataFileMasksPrevStep)
    
    # if jobKey!=initialJobKey:
    #     #if not the intial job
    #     #also need to append the input file types for the next job!
    #     #print(f"TRIGGER: {opts['taskOrder']}")
    #     inputDataFileMasksPrevStep=[a.lower() for a in convListStrToList(jobOptions[jobKey]["inputFiles"])]
    
        
    #Local setting for testing purposes, usually forced to false
    if local:
        j.backend = Local()
        #mooreJob.splitter = SplitByFiles(filesPerJob=5)
        j.outputfiles = [LocalFile(f'*{a}') for a in outputFileTypes]+[LocalFile('stdout'), LocalFile('stderr')]    
    else:
        j.backend = Dirac()

        #if job split is requested, split such that each subjob uses one file
        # (this value can be edited if jobs seem to large)
        if jobOptions[jobKey]["splitJob"]!="False":
            if splitByFile or jobKey!=initialJobKey:
                #we want 1 to 1 input beyond the initial job in a chain
                #if jobOptions[initialJobKey]["splitJob"]!="False":
                j.splitter = SplitByFiles(filesPerJob=1)
            else:
                #only split if not wanting to split by input and it is the bottom level task
                if jobOptions[jobKey]["splitJob"]=="True":
                    #default 10 splits
                    nsplits=10
                    
                else:
                    nsplits=int(jobOptions[jobKey]["splitJob"])

                j.splitter=ArgSplitter(args=[[splitString.format("//%i"%nsplits)] for _ in range(nsplits-1)]+[[splitString.format("//%i"%nsplits),splitStringRemainder.format("%"+"%i"%nsplits)]])

        elif jobKey==initialJobKey and not splitByFile:
            #strange quirk, splitter defaults to 10 splits if there are no input files and we aren't job splitting
            j.splitter=ArgSplitter(args=[['']]) #empty string
        
        #store stdout and stderr locally and any other requested output files as dirac files
        # only foreseen issue here is small files produced such as xmls being stored as dirac files?

        #using dirac file for now for output files!

        ###HACK!! TO KEEP ROOT FILES LOCAL FOR NOW!!!!
        if keepROOTFilesLocal:
            j.outputfiles = [(DiracFile(f'*{a}') if "root" not in a else LocalFile(f'*{a}')) for a in outputFileTypes ]+[LocalFile('stdout'), LocalFile('stderr')]
        else:
            j.outputfiles = [DiracFile(f'*{a}') for a in outputFileTypes ]+[LocalFile('stdout'), LocalFile('stderr')]
        # the std.out can be a local file or a dirac file, not sure if we want to send the std.out to the front end distro

        j.backend.diracOpts = 'j.setTag(["/cvmfs/lhcbdev.cern.ch/"])' 
        #this how to ensure lhcbdev.cern.ch is available at the used sites (forces Tier 1)



#now need to append the transforms in the wrong order!
#keyList.sort(reverse=True)

print("RUNNING TASK: ", t.id)    
t.run()
print("TASK SUCCESSFULLY RUN: ", t.id)
#print(t)
for i in range(1, len(jobOptions)):
    print("TRANSFORM: ", i-1,  jobOptions[f"{0-i}"]['jobTitle'])
print("TRANSFORM: ", i, jobOptions["0"]["jobTitle"])

#jobs?
#print("SUBMITTING JOB: ", jobs[0].id)
#jobs[0].submit()
#print("JOB SUCCESSFULLY SUBMITTED: ", jobs[0].id)

# now run the ganga backend if wanted!
runGangaPollingBackend = False

if runGangaPollingBackend:
    subprocess.Popen(f"python LHCbPR_OTG_GangaBackend_Poll.py {t.id} {jobOptions['newDir']}",shell=True)
