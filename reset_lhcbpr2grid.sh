#!/bin/bash

service lhcbpr2grid stop

rm -rf /var/lib/lhcbpr2grid/*
rm -rf /root/gangadir

rm -rf /root/lhcbpr2grid/lhcbpr2grid_web/db.sqlite3
rm -rf /root/lhcbpr2grid/lhcbpr2grid_web/tasks/migrations

cd /root/lhcbpr2grid/lhcbpr2grid_web
python manage.py makemigrations tasks
python manage.py sqlmigrate tasks 0001
python manage.py migrate

sed -e 's/###HOUR###/'`date +%k | xargs`'/g' /root/lhcbpr2grid/submit_schedule.tmpl.json > /root/lhcbpr2grid/submit_schedule.json

cd /root/lhcbpr2grid
./install-lhcbpr2grid.sh


service lhcbpr2grid start

# python manage.py createsuperuser
