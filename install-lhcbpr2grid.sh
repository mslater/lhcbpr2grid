#/bin/sh

# Create the area in /etc and copy the files
mkdir /etc/lhcbpr2grid
rsync -avz bin /etc/lhcbpr2grid
rsync -avz scripts /etc/lhcbpr2grid

# copy the systemd file
cp lhcbpr2grid.service /etc/systemd/system/.

# copy the ganga config
cp gangarc ~/.gangarc
