# script to run with ganga to monitor jobs

import time

enableMonitoring()
time.sleep(60)

for _ in range(0, 10):
    for j in jobs:
        if not j.status in ['completed', 'submitted', 'running', 'failed', 'new']:
            time.sleep(30)

#disableMonitoring()

for t in tasks:
    for trf in t.transforms:
        if len(trf.getJobs()) > 0:
            print("TASK INFO: {}.{} {}".format(t.id, trf.id, trf.getJobs()[0]))

for j in jobs:
    if not j.status in ['completed', 'submitted', 'running', 'failed', 'new']:
        continue

    job_str = "JOB INFO: {} {} ".format(j.id, j.status)
    for sj in j.subjobs:
        job_str += " DIRACID:{}:{} ".format(sj.backend.id, sj.status)

    print(job_str)


        
