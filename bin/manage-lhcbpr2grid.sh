#!/bin/bash

# shell script to manage lhcbpr2grid as a service rather than a cron job. Could just be an hourly cron if required though

# log everything to the system logger
exec &> >(logger -i -t $0)

# setup variables
STATEDIR=/var/lib/lhcbpr2grid
BINDIR="$(dirname -- $0)"

# setup directories
mkdir -p $STATEDIR
cd $STATEDIR

# setup the LHCb Environment
echo "Setting up LHCb Environment..."
export HOME=/root
source /cvmfs/lhcb.cern.ch/group_login.sh
which lb-run

# proxy
# At present, need to create a new long lived proxy using, e.g:
# lhcb-proxy-init --valid 600:00
# and then copy it to here:
export X509_USER_PROXY=/root/proxy

# main loop for processing requests and monitoring jobs
while :
do
    # grab the schedule
    echo "Grabbing latest copy of the schedule..."
    rm submit_schedule.json
    wget https://gitlab.cern.ch/mslater/lhcbpr2grid/-/raw/master/submit_schedule.json 
    
    # run the full python management script
    python3 $BINDIR/submit-and-monitor-jobs.py

    # sleep for 10mins
    echo "Will sleep for 10mins"
    sleep 600
    
    ## find last hour
    #curr_date=`date +"%Y-%m-%d %H:00:00"`
    
    ## find next hour
    #next_hour_epoch=`date --date="$curr_date 1 hour" +"%s"`
    
    ## sleep until this (+5mins)
    #current_epoch=$(date +%s)
    #sleep_time=$(( $next_hour_epoch + 300 - $current_epoch ))
    
    #echo "Will sleep for $sleep_time"
    #sleep $sleep_time
    
done

