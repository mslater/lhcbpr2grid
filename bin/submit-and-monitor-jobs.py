#!/usr/bin/python3

import configparser
import json
import datetime
import subprocess
import sys
import os
import shutil

# --------------------------------------------------------------------------------------------
#   CONFIG SETUP
# Load in config via JSON
config_file = configparser.ConfigParser()
#config_file.read("/etc/lhcbpr2grid/lhcbpr2grid.conf")
config_file.read("/root/lhcbpr2grid/lhcbpr2grid.conf")
projects_jo_map = config_file["Projects"]["PRJobOptions"]
extract_input_map = config_file["Projects"]["PRExtractInputScript"]
remove_input_opt_map = config_file["Projects"]["PRRemoveInputOption"]

# load current schedule via JSON
schedule = json.load(open("/root/lhcbpr2grid/submit_schedule.json"))

# preserve state dir where we currently are
state_dir = os.getcwd()

# --------------------------------------------------------------------------------------------
#   LOOP OVER TESTS AND CHECK IF WE NEED TO SUBMIT
# Loop over all available tests
for test in schedule['tests']:
    print(test['jobTitle'])
    # find current hour and day
    curr_day = datetime.datetime.now().strftime("%a")
    curr_hour = datetime.datetime.now().hour

    # should we submit?
    if not curr_day in test['days'] or int(test['time']) != curr_hour:
        continue

    # have we submitted this already?
    submit_time = datetime.datetime.now()
    job_title = test['jobTitle'] + submit_time.strftime("_%Y-%m-%d-%H")
    if os.path.exists(os.path.join(state_dir, job_title)):
        continue
    
    # No, collect options and create the submission options
    conf_str = "[JobConfig]\n"
    for key in test:
        if key == "days" or key == "time":
            continue

        conf_str += key + " = {}\n".format(test[key])

    # setup dir for this job and copy scripts over
    #  - note this copies all scripts which is a bit excessive and doesn't scale well. Only required scripts should be copied
    os.chdir(state_dir)
    os.mkdir(job_title)
    os.chdir(job_title)
    script_files = os.listdir(os.path.join(sys.path[0], '..', 'scripts'))
    for file_name in script_files:
        full_file_name = os.path.join(sys.path[0], '..', 'scripts', file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, file_name)

    shutil.copy(os.path.join(sys.path[0], "launchLHCbPRJobOTG.py"), "launchLHCbPRJobOTG.py")
            
    # if this job requires another job, create the required configs
    task_order = 0
    if test['requiresJob']:
        req_job = test['requiresJob']
        while req_job:
            task_order += 1
            for req_test in schedule['tests']:
                if req_job == req_test['jobTitle']:
                    req_submit_time = datetime.datetime.now()
                    req_job_title = req_test['jobTitle'] + req_submit_time.strftime("_%Y-%m-%d-%H")
                    req_conf_str = "[JobConfig]\n"
                    for key in req_test:
                        if key == "days" or key == "time":
                            continue

                        req_conf_str += key + " = {}\n".format(req_test[key])
                        
                    fname = req_test['jobTitle'] + ".conf"
                    open(fname, "w").write(req_conf_str)

                    req_job = req_test['requiresJob']
                    break
            


    # execute the job submission script
    #   - note splitting must be calculated
    fname = job_title + ".conf"
    open(fname, "w").write(conf_str)
    print("Submitting job with config file '{}'".format(fname))
    if task_order > 0:
        print(" ".join(["python3", "launchLHCbPRJobOTG.py", fname]))
        out = subprocess.run(["python3", "launchLHCbPRJobOTG.py", fname], stdout=subprocess.PIPE)
        print (out.stdout.decode("utf-8"))

    else:
        print(" ".join(["python3", "launchLHCbPRJobOTG.py", fname]))
        out = subprocess.run(["python3", "launchLHCbPRJobOTG.py", fname], stdout=subprocess.PIPE)
    
    # if there is a failure, store output and check for GANGAOUTPUT.txt
    if out.returncode:
        print("ERROR Submitting '{}'. Check web interface for more info.".format(job_title))
        err_str = "SCRIPT OUTPUT:\n" + out.stdout.decode("utf-8") 

        # add GANGAOUTPUT.txt
        if os.path.exists(os.path.join(test['newDir'], "GANGAOUT.txt")):
            err_str += "\nGANGAOUT.txt:\n" + open(os.path.join(test['newDir'], "GANGAOUT.txt")).read()

        # write out to the task state
        # STAGES
        # DIRAC JOBS
        state = {"task_name": test['jobTitle'], "curr_status":"task failed", "ganga_task_id":-1, "status_info":"the Task creation/job submission failed", "stages":[], "task_submit_date":submit_time.strftime("%Y-%m-%d-%H-%M")}
        state["stages"].append( {"stage_name":job_title, "stage_submit_date":submit_time.strftime("%Y-%m-%d-%H-%M"), "curr_status":"none", "ganga_job_id":-1, "ganga_transform_id":-1, "status_info":err_str} )
        state["stages"][0]["dirac_jobs"] = []
        
        open("task_state.json", "w").write(json.dumps(state))
    else:
        
        # submission was successful. extract job ID from GANGAOUTPUT.txt and write out JSON
        out_str = "\nGANGAOUT.txt:\n" + open(os.path.join(test['newDir'], "GANGAOUT.txt")).read()
        task_id = -1
        job_id = -1
        trf_id = []
        for ln in out_str.split('\n'):
            if ln.find("JOB SUCCESSFULLY SUBMITTED:") > -1:
                trf_id.append([job_title, int(ln.split()[-1])])

            if ln.find("TASK SUCCESSFULLY RUN:") > -1:
                task_id = int(ln.split()[-1])

            if ln.find("TRANSFORM:") > -1:
                trf_id.append([ln.split()[2],  int(ln.split()[1])])
                                
        # state goes straight to 'jobs running' as we now have jobs submitted
        state = {"task_name": test['jobTitle'], "curr_status":"jobs running", "ganga_task_id":task_id, "status_info":"waiting on dirac jobs to finish", "stages":[], "task_submit_date":submit_time.strftime("%Y-%m-%d-%H-%M")}

        for trf_info in trf_id:
            if task_id > -1:
                state["stages"].append( {"stage_name":trf_info[0], "stage_submit_date":submit_time.strftime("%Y-%m-%d-%H-%M"), "curr_status":"submitted" if len(state["stages"]) == 0 else "waiting", "ganga_job_id":-1, "ganga_transform_id":trf_info[1], "status_info":out_str, "dirac_jobs":[]} )
            else:
                state["stages"].append( {"stage_name":trf_info[0], "stage_submit_date":submit_time.strftime("%Y-%m-%d-%H-%M"), "curr_status":"submitted", "ganga_job_id":job_id, "ganga_transform_id":-1, "status_info":out_str} )
                state["stages"][0]["dirac_jobs"] = []

                

        open("task_state.json", "w").write(json.dumps(state))
    

# --------------------------------------------------------------------------------------------
#   MONITOR JOBS
# start up Ganga to allow monitoring
try:
    ganga_out = subprocess.run(["ganga", os.path.join(sys.path[0], "ganga-monitor-script.py")], stdout=subprocess.PIPE, timeout=600).stdout.decode("utf-8")
except subprocess.TimeoutExpired:
    # Process took longer than the specified timeout
    print("Timeout expired. Subprocess execution was terminated.")
    ganga_out = "TIMEOUT HIT"
    
# store monitoring log - should store in the DB so the web interface can access it
os.chdir(state_dir)
open("GangaMonitoringLog"+datetime.datetime.now().strftime("-%Y-%m-%d-%H-%M.log"), "w").write(ganga_out)
     
# loop over tasks dirs and update status file for tasks
for task_name in os.listdir(state_dir):
    task_dir = os.path.join(state_dir, task_name)

    if not os.path.exists( os.path.join(task_dir, "task_state.json") ):
        continue
    
    # load the json
    task_state = json.loads(open( os.path.join(task_dir, "task_state.json") ).read())
    if task_state['curr_status'] in ['task failed', 'complete']:
        continue

    # grab info from monitoring
    for stage_state in task_state['stages']:

        for ln in ganga_out.split('\n'):
            if ln.find("JOB INFO:") > -1:
                toks = ln.split()
                ganga_job_id = int(stage_state['ganga_job_id'])

                if int(toks[2]) != ganga_job_id:
                    continue
            
                stage_state['curr_status'] = toks[3]

                # store dirac ids
                stage_state['dirac_jobs'] = []
                for dirac_id in toks[4:]:
                    stage_state['dirac_jobs'].append({'dirac_id':int(dirac_id.split(':')[1]), 'job_status':dirac_id.split(':')[2]})
            elif ln.find("TASK INFO:") > -1:
                toks = ln.split()
                task_id = int(toks[2].split('.')[0])
                trf_id = int(toks[2].split('.')[1])
                
                if task_id != task_state["ganga_task_id"] or trf_id != stage_state["ganga_transform_id"]:
                    continue

                stage_state['ganga_job_id'] = int(toks[3])
                
                    
    # update task status
    task_status = "jobs complete"
    task_info = "all dirac jobs in all stages have completed"
    for stage_state in task_state['stages']:
        if stage_state['curr_status'] == 'failed':
            task_status = "jobs failed"
            task_info = "some jobs have failed"
            break
        elif stage_state['curr_status'] in ['submitted', 'running']:
            task_status = "jobs running"
            task_info = "waiting on dirac jobs to finish"

    task_state['curr_status'] = task_status
    task_state['status_info'] = task_info
    
    # save the json
    open(os.path.join(task_dir, "task_state.json"), "w").write(json.dumps(task_state))
    
# Any tasks marked completed in Ganga, fork a process to do the following:
#  - download data in to subfolder of task folder
#  - Run Handler appropriately
#  - zip up and upload outputs

# Loop over folders to check on Task status for forked processes

# update DB from dir info
import django
import sys
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'lhcbpr2grid_web.settings')
sys.path.append("/root/lhcbpr2grid/lhcbpr2grid_web")
django.setup()

from tasks.models import Task

for task_name in os.listdir(state_dir):
    task_dir = os.path.join(state_dir, task_name)
    
    if not os.path.exists( os.path.join(task_dir, "task_state.json") ):
        continue

    # load the json
    task_state = json.loads(open( os.path.join(task_dir, "task_state.json") ).read())

    # do we already have this task?
    if len(Task.objects.filter(task_name = task_state['task_name'], task_submit_date = datetime.datetime.strptime(task_state['task_submit_date'], "%Y-%m-%d-%H-%M"))) > 0:
        
        # update from json
        db_task = Task.objects.get(task_name = task_state['task_name'], task_submit_date = datetime.datetime.strptime(task_state['task_submit_date'], "%Y-%m-%d-%H-%M"))
        db_task.curr_status = task_state['curr_status']
        db_task.ganga_task_id = int(task_state['ganga_task_id'])
        db_task.status_info = task_state['status_info']

        for db_stage in db_task.stage_set.all():
            for stage_state in task_state['stages']:
                if stage_state['stage_name'] == db_stage.stage_name:
                    db_stage.curr_status = stage_state['curr_status']
                    db_stage.status_info = stage_state['status_info']
                    
                    # do we have dirac jobs yet?
                    if len(stage_state['dirac_jobs']) == 0:
                        db_stage.num_dirac_jobs = 0
                        db_stage.save()
                        continue
                    
                    if db_stage.num_dirac_jobs == 0:
                        # create dirac job entries
                        for diracjob in stage_state['dirac_jobs']:
                            db_diracjob = db_stage.diracjob_set.create(dirac_id = diracjob['dirac_id'], job_status = diracjob['job_status'])
                            db_diracjob.save()
                    else:
                        for db_diracjob in db_stage.diracjob_set.all():
                            for diracjob in stage_state['dirac_jobs']:
                                if diracjob['dirac_id'] == db_diracjob.dirac_id:
                                    db_diracjob.job_status = diracjob['job_status']
                                    db_diracjob.save()

                    db_stage.num_dirac_jobs = len(stage_state['dirac_jobs'])
                    db_stage.save()

        db_task.save()

        
    else:
    
        # create the task and save
        db_task = Task(task_name = task_state['task_name'], task_submit_date = datetime.datetime.strptime(task_state['task_submit_date'], "%Y-%m-%d-%H-%M"), curr_status = task_state['curr_status'], ganga_task_id = int(task_state['ganga_task_id']), status_info = task_state['status_info'])
        db_task.save()
    
        for stage_state in task_state['stages']:
            db_stage = db_task.stage_set.create( stage_name = stage_state['stage_name'], stage_submit_date = datetime.datetime.strptime(stage_state['stage_submit_date'], "%Y-%m-%d-%H-%M"), ganga_job_id = int(stage_state['ganga_job_id']), ganga_transform_id = int(stage_state['ganga_transform_id']), curr_status = stage_state['curr_status'], num_dirac_jobs = len(stage_state['dirac_jobs']), status_info = stage_state['status_info'])
            db_stage.save()

            for diracjob in stage_state['dirac_jobs']:
                db_diracjob = db_stage.diracjob_set.create(dirac_id = diracjob['dirac_id'], job_status = diracjob['job_status'])
                db_diracjob.save()


# State Machine Info:
# Task:   task submitted    - Task/Job created in Ganga
#         task failed       - the Task creation/job submission failed
#         jobs running      - waiting on dirac jobs to finish
#         jobs failed       - some jobs have failed
#         jobs complete     - all dirac jobs in all stages have completed
#         merging           - downloading and merging data
#         merge failed      - download or merge failed
#         running handlers  - jobs handlers are being run on the downloaded and merged data
#         handlers failed   - running the job handlers failed
#         complete          - all jobs completed, handlers run and outputs uploaded
#         upload failed     - the upload of the outputs from the handlers failed
# Stage:  none, waiting, submitted, running, completed, failed - mapped directly from Ganga main job for the stage
