# script to run with ganga to monitor jobs

import time

#enableMonitoring()
for j in jobs:
    print(">>>>>>>>>>>>>>>>>>   START JOB ID: {}  <<<<<<<<<<<<<<<<<<".format(j.id))
    if not j.status in ['completed', 'failed']:
        print("---- Monitoring Started")
        ret = runMonitoring(jobs.select(minid=j.id, maxid=j.id))
        if ret:
            print("---- Monitoring Stopped (OK)")
        else:
            print("---- Monitoring Stopped (NOT OK)")

    print("---- Job info")
    job_str = "JOB INFO: {} {} ".format(j.id, j.status)
    for sj in j.subjobs:
        job_str += " DIRACID:{}:{} ".format(sj.backend.id, sj.status)

    print(job_str)

    print(">>>>>>>>>>>>>>>>>>   END JOB ID: {}  <<<<<<<<<<<<<<<<<<".format(j.id))

        
