# LHCbPR2Grid

## Process of building the Django site

```
cd /root/lhcbpr2grid
django-admin startproject lhcbpr2grid_web
python manage.py startapp tasks
```

Created basic view in `tasks/views.py` and url in `tasks/urls.py` and `lhcbpr2grid_web/urls.py`.
Added app to `lhcbpr2grid_web/settings.py`

Created the migration scripts using:
```
python manage.py makemigrations tasks
```

Actually migrated using:
```
python manage.py migrate
```
